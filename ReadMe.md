# Task
Given [flag enum](https://www.alanzucconi.com/2015/07/26/enum-flags-and-bitwise-operators/) `DaysOfWeekAppreviation` which indicates working days during a week. Need to implement conversion to string by following rules.

## Rules
* Empty string for None value
* Intervals of consecutive days should be divided by comma
* Intervals of consecutive days should be represended via Start-End pattern
* First day of a week is Sunday

## Examples
| Given | Then |
|-------|------|
| Mon | "Mon" |
| Mon and Sat | "Mon,Sat" |
| from Sunday to Wednesday | "Sun-Wed" |
| from Sunday to Wednesday and Friday | "Sun-Wed,Fri" |

# Tests
[xunit](https://xunit.github.io/) is a platform used as test runner and [fluent assertions](https://fluentassertions.com/) is a library used as for writing assertions for unit-tests.

See [Theory and MemberData attributes](http://hamidmosalla.com/2017/02/25/xunit-theory-working-with-inlinedata-memberdata-classdata/) used for providing data as test cases.