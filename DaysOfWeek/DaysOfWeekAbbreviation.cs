﻿using System;

namespace DaysOfWeek
{
    [Flags]
    public enum DaysOfWeekAbbreviation
    {
        None = 0,

        Sun = 1 << 0,
        Mon = 1 << 1,
        Tue = 1 << 2,
        Wed = 1 << 3,
        Thu = 1 << 4,
        Fri = 1 << 5,
        Sat = 1 << 6
    }
}