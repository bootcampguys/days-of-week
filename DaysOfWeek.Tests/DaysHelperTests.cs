﻿using System.Collections.Generic;
using FluentAssertions;
using Xunit;
using Days = DaysOfWeek.DaysOfWeekAbbreviation;

namespace DaysOfWeek.Tests
{
    public class DaysHelperTests
    {
        [Theory, MemberData(nameof(GetToStringTestData))]
        public void ToString_Test(Days days, string expectedResult)
        {
            // Act
            var result = DaysHelper.ToString(days);

            // Assert
            result.Should().BeEquivalentTo(expectedResult);
        }

        public static IEnumerable<object[]> GetToStringTestData()
        {
            yield return new object[] { Days.None, string.Empty };
        }
    }
}